= 省リソースで動作するビデオカンファレンスソフトウェアのGalène
Kenichiro Matohara(matoken) <maroken@kagolug.org>
:revnumber: 1.0
:revdate: 2022-06-19(sun)
:revremark: 「{doctitle}」
:homepage: https://matoken.org/
:imagesdir: resources
:data-uri:
:example-caption: 例
:table-caption: 表
:figure-caption: 図
:backend: revealjs
:revealjs_theme: serif
:customcss: resources/my-css.css
:revealjs_slideNumber: c/t
:title-slide-transition: none
:icons: font
:revealjs_hash: true
:revealjs_center: true
:revealjs_autoPlayMedia: true
:revealjs_transition: false
:revealjs_transitionSpeed: fast

== link:https://matoken.org[Kenichiro Matohara(matoken) https://matoken.org]

image::map.jpg[background, size=cover]

* 南隅から参加(鹿児島の右下)
* 好きなLinuxディストリビューションはDebian
* お仕事募集 mailto:work＠matohara.org

== 最近

* スライド共有サービスのSlideShareの無料アカウント100スライド制限に
** link:https://gitlab.com/matoken/koedolug-2022.06/-/blob/main/slide/slide.adoc[スライドファイルをセルフホスト(Pelican利用)]
* 色々壊れる><
** ドッキングステーションのUSBポート，USB HDD，NotePC不安程度Up
** 草払い機のエンジン(修理代500k!)

== 省リソースで動作するビデオカンファレンスソフトウェアのGalène

== ビデオミーティング，カンファレンスできればOSSで賄いたい

* 現在プロプラエタリなDiscordを利用
** 本来10人まで -> COVID-19だし25人までいいよ -> いつまで?
* OSSなビデオミーティングソフトウェア
** セルフホストするには重い

== ビデオミーティングソフトウェアのserver requirements

link:https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-requirements/[Jitsi Meet]footnote:[どんどん増えている……?]::
// * Network 100Mbps〜10Gbps
* RAM 2〜8GB
* CPU 4core〜
link:https://docs.bigbluebutton.org/2.5/install.html#minimum-server-requirements[BigBlueButton]::
* RAM 16GB〜
* CPU 8Core〜

=== 以前

* RAM 1GBのVPSや4GBのVPSで解像度などを落としたJitsi MeetやNextcloud Talkで討ち死に
* 動かすとしても時間課金で利用時だけ借りる感じ

== 音声だけなら軽量だけど……

* 以前ここ(鹿児島らぐ)でも使ったことがあるMumble
** サーバクライアントともに軽量で動作も問題ない
** クライアントが必要だが，Webベースのクライアントも存在する
** ビデオ対応も話し合われているけど今の所実装の予定はなさそう
*** link:https://github.com/mumble-voip/mumble/issues/4150[Video feature discussion · Issue #4150 · mumble-voip/mumble]
* しかし画面共有機能も欲しい

== Galène

====
Galene needs little momory -- it should run correctly in 128MB of RAM, and is rock solid with 512MB. 
====

link:https://galene.org/faq.html[Galène FAQ]

桁違いに軽量そう!  +
(PINE64あたりだとAES acceleratorが乗ってたはず)

== link:https://galene.org/[Galène videoconference server]

* 2020年末から開発されているビデオカンファレンスソフトウェア． Go製MITライセンス．
** WebRTC部分はlink:https://pion.ly/[Pion] を利用
* **サーバ利用メモリ128MB〜**
//* Linux/amd64, Linux/arm64でテストされているが，Linux/armv7, Linux/mips(OpenWRT), Windowsでも動作
* Galèneはエンドツーエンド暗号化はされない．サーバーで権限があればデータを覗き見ることが可能
** link:https://bloggeek.me/webrtcglossary/sfu/#[SFU(Selective Forwarding Unit)]
** 帯域増えそう
* 公式テストサーバ有り: https://galene.org:8443/

=== link:https://bloggeek.me/webrtcglossary/sfu/#[SFU(Selective Forwarding Unit)]

image:SFU.jpg[]

== 早速試す

----
$ sudo apt install golang
$ git clone https://github.com/jech/galene
$ cd galene
$ CGO_ENABLED=0 go build -ldflags='-s -w'
----

NOTE: 要Go 1.13以降

=== クロスビルド例

----
$ GOARCH="arm" GOOS="linux" GOARM=6 CGO_ENABLED=0 \
  go build -o galene_arm6_linux -ldflags='-s -w' <.>
$ GOARCH="arm" GOOS="linux" GOARM=7 CGO_ENABLED=0 \
  go build -o galene_arm7_linux -ldflags='-s -w' <.>
$ GOARCH="arm64" GOOS="linux" CGO_ENABLED=0 \
  go build -o galene_arm64_linux -ldflags='-s -w' <.>
----

<.> Linux arm6(Raspberry Pi 1)
<.> Linux arm7(Raspberry Pi 2〜)
<.> Linux arm64(Raspberry Pi 2 B 1.2〜かつ64bit OS)

NOTE: 今回Linux/amd64とLinux/armhfで試した，Linux/arm64，Linux/mipsやWindowsでも動くらしい

== グループを作成

テスト用グループ(部屋)の設定を作成

----
$ mkdir groups
$ vi groups/name.json
----

.groups/name.json
----
{
    "op": [{"username": "admin", "password": "1234"}],
    "presenter": [{}]
}
----

== 動作確認

----
$ ./galene &
----

https://localhost:8443/ で動作確認

== top page

image::Galène_top.jpg[]

== login

image::Galène_login.jpg[]

== Group

image::Galène_group.jpg[]

== setting

image:Galène_setting01.jpg[]
image:Galène_setting02.jpg[]

=== setting(1 of 2)

Media Options::
* Camera: → off or 各種カメラ
* Microphone: → off or 各種マイク
* ✅ Mirror view
* □ Blackboard mode (高解像度低fps)になる
* ✅ Noise suppression
* □ High-quality audio (音楽再生時など)

=== setting(2 of 2)

Other Settings::
* Filter: → none, Holizontal mirror, Virtical mirror
* Send: → lowest, low, normal, unlimited
* Simulcast: → off, auto, on(link:https://bloggeek.me/webrtcglossary/simulcast/[サイマルキャスト])
* Receive: → noting, audio only, screen share(low), screen share, everything(low), everything
* □ Activity detection (????) footnote:[Firefoxでは動作しない]

== chatでのコマンド

* /help ヘルプ表示
* /leave グループを離れる
* /msg プライベートメッセージを送信
* /presentfile 動画音声ファイルのブロードキャスト再生
* /raise 挙手(メンバーリストの○が🖐になる)
* /renegotiate メディアストリームの再ネゴシエート
* /sendfile 指定したユーザへのファイル送信(IPアドレスも開示される)
* /unraise 手を下げる

// NOTE: いくつかの機能はユーザ一覧のユーザ名をクリックすることでも使える

== helpに出てこないチャットコマンド

* /record 録画開始footnote:[グループの設定で `allow-recording` が必要]
* /unrecord 録画終了
* /set resolution [1024, 768] 送信解像度の指定，指定後ビデオの再起動(Disable/Enable)が必要

== Raspberry Pi 3B + Raspberry Pi OS bullseye armhf

image:Galène_htop.jpg[]

== 少し使ってみて

* LAN内でRaspberry Pi 3 model B + Raspberry Pi OS bullseye armhf(4core/1GB RAM)をサーバにして3台接続
* 半日ほど放置しても安定して動作していそう
* ビデオや画面共有を増やしてもサーバの負荷はあまり上がらない
* クライアントの使い勝手は慣れが必要そう

== リモートサーバにデブロイ

----
$ rsync -avcP galene static galene@server.example.org:~ <.>
$ ssh galene@server.example.org <.>
$ mkdir -p data groups <.>
$ sudo install -m 400 -o `id -u` -g `id -g` /etc/letsencrypt/live/server.example.org/fullchain.pem data/cert.pem <.>
$ sudo install -m 400 -o `id -u` -g `id -g` /etc/letsencrypt/live/server.example.org/privkey.pem data/key.pem
----

<.> Galèneバイナリとstaticディレクトリをコピー
<.> リモートサーバに移動
<.> 必要なディレクトリ作成
<.> 証明書コピーfootnote:[証明書ファイルがない場合起動時に毎回自己証明書が作られる]

== ファイアウォールの設定

最低でも TCP 8443 が必要

TCP:: 1194, 8443
UDP:: 1194, エフェメラルポート(or -udp-range range)

==  グローバル設定ファイル

.data/config.json
----
{
    "canonicalHost": "galene.example.org",
    "admin":[{"username":"root","password":"secret"}]
}
----

NOTE: `canonicalHost` を間違えるとブラウザにキャッシュされてしまい面倒><

== グループ設定

.groups/groupname.json
----
{
    "op": [{"username":"jch","password":"1234"}],
    "presenter": [{}],
    "allow-recording": true,
    "allow-anonymous": true,
    "allow-subgroups": true,
    "public": true,
    "displayName": "Galenサンプル",
    "contact": "galene@example.org",
    "comment": "コメント",
    "codecs": ["vp8", "opus"]
}
----

詳細は `README` を参照のこと

=== ユーザーの種類

op:: グループ管理者
presenter:: カメラやマイクを有効に出来る一般ユーザ
other:: 見聞きするだけのリスナー

== 実行

----
$ ulimit -n 65536
$ nohup ./galene &
----

常用する場合は起動scriptやSystemdでの起動を設定

== 関連ソフトウェア

* link:https://github.com/garage44/pyrite[garage44/pyrite: Pyrite is a web(RTC) client for the Galène videoconference server.]
**  Vueフレームワークの代替フロントエンド
* link:https://github.com/erdnaxe/galene-stream[erdnaxe/galene-stream: Gateway to send UDP, RTMP or SRT streams to Galène videoconference server.]
** UDP, RTMP, またはSRTストリームをGalèneに送信するためのゲートウェイ(Gstreamer利用)
* link:https://github.com/jech/galene-sample-auth-server[jech/galene-sample-auth-server]
** サンプル認証サーバ

=== フロントエンドのPyriteを試す

image:logo-text.svg[]

* Docker(Galène同梱)かnpx, npmで導入
** 今回はDockerで試した
*** Disk 1.2GBほど必要footnote:[Dockerを使わないGalène単体は11MB程]

----
$ git clone https://github.com/garage44/pyrite
$ cd pyrite/docker
$ PYRITE_UID=`id -u` PYRITE_GID=`id -g` docker-compose up
$ xdg-open http://localhost:3030/
----

=== Pirite画面

image::pirite01.jpg[]

=== Piriteユーザー接続画面

image::pirite02.jpg[width=70%]

=== Pirite管理者でグループ編集

image::pirite03.jpg[width=66%]

//NOTE: Pyrite管理者ID/Passwordは `./docker/pyrite/config/.pyriterc` 内のname/password

=== Pirite

* 見た目が今どき
* i18n対応(今の所英/仏/独)や2種類のテーマがある
* ビデオ，マイクの確認もしやすい
* ユーザー，グループの管理がWebで出来るのが便利
* node.js導入済みなら導入しても良さそう

== まとめ

* サーバが軽量で使いやすそうなGalène
* ローカライズされていないなど使いづらい部分はあるけど普通に使えそう
* 機能は少し少ないけど必要十分
* 軽量なのでFreedomboxなどでホームサーバにも良さそう
* インターネット上に用意したので後で試してみましょう

== みんなでGalèneを試してみた

* イベント終了後のアフターで6時間近く使ってみた
* 思った以上に安定して使えた
* サーバにはさくらのVPS(1GB)を利用footnote:[鹿児島らぐのサーバはさくらインターネットによって提供されています]
** サーバのスペックも余裕
* バグ?を発見
** 画面共有状態でGalèneではなくウェブブラウザ側の画面共有の停止を行うとウェブブラウザが落ちるfootnote:[ChromiumベースのBrave 1.39.122]

=== サーバのリソースモニタ
//footnote:[17:00過ぎから23時近くまでGalèneを利用した]

image::serverresource.jpg[width=66%]

=== クライアントのネットワーク利用状況

----
$ vnstat -h -b "2022-06-19 14:00" -e "`date +%F\ %H:%M`"

 wlp4s0  /  hourly

         hour        rx      |     tx      |    total    |   avg. rate
     ------------------------+-------------+-------------+---------------
     2022-06-19
         14:00     66.80 MiB |  162.45 MiB |  229.25 MiB |  534.20 kbit/s
         15:00     67.95 MiB |  124.63 MiB |  192.58 MiB |  448.75 kbit/s
         16:00    101.91 MiB |   22.53 MiB |  124.45 MiB |  289.98 kbit/s
         17:00    237.75 MiB |   98.66 MiB |  336.41 MiB |  783.89 kbit/s
         18:00    210.19 MiB |   76.22 MiB |  286.41 MiB |  667.38 kbit/s
         19:00    628.30 MiB |  143.67 MiB |  771.97 MiB |    1.80 Mbit/s
         20:00    871.78 MiB |  113.51 MiB |  985.29 MiB |    2.30 Mbit/s
         21:00    100.20 MiB |   32.89 MiB |  133.09 MiB |  310.12 kbit/s
         22:00     79.40 MiB |   28.66 MiB |  108.06 MiB |  274.70 kbit/s
     ------------------------+-------------+-------------+---------------
      sum of 9      2.31 GiB |  803.22 MiB |    3.09 GiB |
----

NOTE: 17:00過ぎからGalène利用，接続人数は減ったがデータ量は増えているところがある
// ** 恐らくSFUのせい
// ** 通信量が厳しい場合は各ユーザでオーディオのみ，オーディオ+画面共有のみなどの設定が可能

== 奥付

発表::
* link:https://kagolug.connpass.com/event/251151/[鹿児島Linux勉強会 2022.06(オンライン開催)]
発表者::
* link:https://matoken.org/[Kenichiro Matohara(matoken)]
利用ソフトウェア::
* link:https://github.com/asciidoctor/asciidoctor-reveal.js[Asciidoctor Reveal.js]
ライセンス::
* link:https://creativecommons.org/licenses/by/4.0/[CC BY 4.0]

